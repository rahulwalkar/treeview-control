module MyJsTreesHelper
  #Cycles round each root node
  def get_children_for_root(all_ous)
      
    boo = {}
      
      boo = add_children(1, all_ous)
      
    return boo
  end
    
  def add_children(parent_id, all_ous)
      
      data = {:data => (get_OU_Name(parent_id))}
      data[:children] = children = []
      
      #cycle round all the ous and check if they are child of the parent_id
      all_ous.each_with_index do |ou, index|
          
          if(parent_id == ou.Parent_ID)
              
              children << add_children(ou.id, all_ous)
              
          end
      end
      #clear up empty children
      if children.empty?
          data.delete(:children)
      end
      return data
  end
    
  def get_OU_Name(id)
      @ou = MyJsTree.find(id)
      
      return @ou.OUName
  end
end
