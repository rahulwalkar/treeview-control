class MyJsTreesController < ApplicationController
  before_action :set_my_js_tree, only: [:show, :edit, :update, :destroy]

  # GET /my_js_trees
  # GET /my_js_trees.json
    
  def jstreedata
      
      @all_ous = MyJsTree.all
      
      respond_to do |format|
          format.json
      end
  end
      
    
  def jstree
    respond_to do |format|
      format.html
    end
  end
      
      
  def index
    @my_js_trees = MyJsTree.all
  end

  # GET /my_js_trees/1
  # GET /my_js_trees/1.json
  def show
  end

  # GET /my_js_trees/new
  def new
    @my_js_tree = MyJsTree.new
  end

  # GET /my_js_trees/1/edit
  def edit
  end

  # POST /my_js_trees
  # POST /my_js_trees.json
  def create
    @my_js_tree = MyJsTree.new(my_js_tree_params)

    respond_to do |format|
      if @my_js_tree.save
        format.html { redirect_to @my_js_tree, notice: 'My js tree was successfully created.' }
        format.json { render action: 'show', status: :created, location: @my_js_tree }
      else
        format.html { render action: 'new' }
        format.json { render json: @my_js_tree.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /my_js_trees/1
  # PATCH/PUT /my_js_trees/1.json
  def update
    respond_to do |format|
      if @my_js_tree.update(my_js_tree_params)
        format.html { redirect_to @my_js_tree, notice: 'My js tree was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @my_js_tree.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /my_js_trees/1
  # DELETE /my_js_trees/1.json
  def destroy
    @my_js_tree.destroy
    respond_to do |format|
      format.html { redirect_to my_js_trees_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_my_js_tree
      @my_js_tree = MyJsTree.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def my_js_tree_params
      params.require(:my_js_tree).permit(:OUName, :Parent_ID)
    end
end
