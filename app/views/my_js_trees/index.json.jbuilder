json.array!(@my_js_trees) do |my_js_tree|
  json.extract! my_js_tree, :id, :OUName, :Parent_ID
  json.url my_js_tree_url(my_js_tree, format: :json)
end
