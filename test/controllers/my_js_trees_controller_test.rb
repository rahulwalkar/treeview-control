require 'test_helper'

class MyJsTreesControllerTest < ActionController::TestCase
  setup do
    @my_js_tree = my_js_trees(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:my_js_trees)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create my_js_tree" do
    assert_difference('MyJsTree.count') do
      post :create, my_js_tree: { OUName: @my_js_tree.OUName, Parent_ID: @my_js_tree.Parent_ID }
    end

    assert_redirected_to my_js_tree_path(assigns(:my_js_tree))
  end

  test "should show my_js_tree" do
    get :show, id: @my_js_tree
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @my_js_tree
    assert_response :success
  end

  test "should update my_js_tree" do
    patch :update, id: @my_js_tree, my_js_tree: { OUName: @my_js_tree.OUName, Parent_ID: @my_js_tree.Parent_ID }
    assert_redirected_to my_js_tree_path(assigns(:my_js_tree))
  end

  test "should destroy my_js_tree" do
    assert_difference('MyJsTree.count', -1) do
      delete :destroy, id: @my_js_tree
    end

    assert_redirected_to my_js_trees_path
  end
end
