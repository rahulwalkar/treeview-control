class CreateMyJsTrees < ActiveRecord::Migration
  def change
    create_table :my_js_trees do |t|
      t.string :OUName
      t.integer :Parent_ID

      t.timestamps
    end
  end
end
